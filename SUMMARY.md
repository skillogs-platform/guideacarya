# Sommaire

## Guide d'utilisateur

- [Première connexion à la plateforme](./connexion/README.md)
- [Réinitialisation de son mot de passe](./reinitialisation/README.md)

## Site internet

- [Skillogs](https://skillogs.com)