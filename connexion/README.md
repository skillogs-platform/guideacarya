# Première connexion à la plateforme Acarya

Vous venez de recevoir un mail comme suit, votre compte **`Skillogs`** est donc créé, il ne manque plus que votre mot de passe à définir.:

**`ATTENTION`** ce mail peut arriver dans vos SPAMS:

![mail](mailBienvenue.png)

Vous pouvez cliquer soit :
- sur le bouton **CREER VOTRE MOT DE PASSE** (`indication 1`)
- sur le lien (`indication 2`)

![mail](mailBienvenueIndic.png)

Cette action vous envoie sur une page internet comme ci-dessous :

![reset](../commun/mdpForm.png)

Vous devez ensuite :
- remplir le formulaire avec le mot de passe que vous souhaitez associer à votre compte **`Skillogs`** (`indication 1`)
- puis cliquer sur le bouton **Sauvegarder le mot de passe** (`indication 2`)

![reset](../commun/mdpFormRempli.png)

Une nouvelle page vous confirmera l'enregistrement du mot de passe.

![reset](../commun/mdpChange.png)

Cliquez ensuite sur le lien **Retour à la page de connexion**.

![retour](../commun/retourConnexion.png)

Vous êtes redirigé sur la page de connexion d'`Acarya`.

Renseignez votre email de compte **`Skillogs`** et le mot de passe que vous venez de créer.

- email de compte (`indication 1`)
- mot de passe (`indication 2`)
- cliquer sur le bouton **Connexion** (`indication 3`)

![connexion](connexionRempli.png)

Vous êtes bien connecté à la plateforme. Félicitations !

![dashboard](dashboard.png)

